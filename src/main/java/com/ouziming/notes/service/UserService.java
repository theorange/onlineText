package com.ouziming.notes.service;

import com.ouziming.notes.dao.UserDao;

/**
 * @author ou`zi`ming
 * @date 2023年04月16日22时
 * discription 用户服务
 */
public class UserService {
    /**
     * 登录服务
     * @param account 账号
     * @param password 密码
     * @return 用户编号
     */
    public static int loginCheck(String account,String password){
        //返回0就检验失败，返回用户id则校验成功
        return UserDao.selectIdFromAccountAndPassword(account, password);
    }

    /**
     * 创建用户
     * @param userName 昵称
     * @param accoount 账号
     * @param password 密码
     * @param enrollTime 注册时间
     * @param headShot 头像字节数组
     * @param email 邮箱
     * @return 注册成功会返回1
     */
    public static int addUser(String userName,String accoount,String password,Long enrollTime,byte[] headShot,String email){
        return UserDao.addUser(userName, accoount, password, enrollTime, headShot, email);
    }
}
