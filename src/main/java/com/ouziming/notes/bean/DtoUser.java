package com.ouziming.notes.bean;

/**
 * @author ou`zi`ming
 * @date 2023年04月16日19时
 * discription
 */
public class DtoUser {
    int userId;
    String name;
    byte[] headShot;
    double money;
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getHeadShot() {
        return headShot;
    }

    public void setHeadShot(byte[] headShot) {
        this.headShot = headShot;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
