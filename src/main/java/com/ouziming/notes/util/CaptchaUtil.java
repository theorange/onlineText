package com.ouziming.notes.util;

import java.util.Random;

/**
 * @author ou`zi`ming
 * @date 2023年04月17日13时
 * discription 关于验证码的工具类
 */
public class CaptchaUtil {
    /**
     * 生成验证码
     * @return 随机生成的验证码
     */
    public static String generate(){
        String code = "";
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            code += random.nextInt(10);
        }
        return code;
    }
}
