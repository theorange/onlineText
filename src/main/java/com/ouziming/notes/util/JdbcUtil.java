package com.ouziming.notes.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author ou`zi`ming
 * @date 2023年04月16日18时
 * discriptionJDBC工具类
 */
public class JdbcUtil {
    /**
     * JDBC获取Connection对象
     * @return Connection对象
     */
    public static Connection getConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/shop", "root", "123456");
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
}
