package com.ouziming.notes.util;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author ou`zi`ming
 * @date 2023年04月17日17时
 * discription
 */
public class EmailUtil {
    /**
     * 发送邮件的方法
     * @param toEmail 收件人邮箱
     * @param code 验证码
     * @return 是否发送成功
     */
    public static boolean send(String toEmail,String code){
        boolean flag = false;
        //发送人的邮箱和密码
        final String fromEmail = "1043435944@qq.com";
        final String password = "nagcqrxkxiqvbaia";
        //配置java.mail基本属性
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.qq.com");
        props.put("mail.smtp.port", "587");
        //创建认证器
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail,password);
            }
        };
        //创建会话
        Session session = Session.getInstance(props,auth);
        try {
            //设置邮件信息
            MimeMessage message = new MimeMessage(session);
            message.setFrom(fromEmail);
            message.setRecipients(Message.RecipientType.TO,toEmail);
            message.setSubject("验证码");
            message.setText(code);
            //发送邮件
            Transport.send(message);
        }catch (MessagingException e){
            e.printStackTrace();
        }
        flag = true;
        return flag;
    }
}
