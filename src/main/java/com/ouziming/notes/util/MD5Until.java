package com.ouziming.notes.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author ou`zi`ming
 * @date 2023年04月16日20时
 * discription 密码加密
 */
public class MD5Until {
    /**
     * 加密工具类
     * @param input 要加密的信息
     * @return 加密后的信息
     */
    public static String md5(String input){
        try {
            //创建MD5加密对象
            MessageDigest md = MessageDigest.getInstance("MD5");
            //计算MD5加密的值
            md.update(input.getBytes(StandardCharsets.UTF_8));
            byte[] digest = md.digest();
            //将byte数组转化为字符串
            BigInteger bigInteger = new BigInteger(1,digest);
            //返回加密后的结果
            return bigInteger.toString(16);
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
            return null;
        }
    }
}
