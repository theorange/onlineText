package com.ouziming.notes.controller;

import com.ouziming.notes.util.CaptchaUtil;
import com.ouziming.notes.util.EmailUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author ou`zi`ming
 * @date 2023年04月18日15时
 * discription
 */
@WebServlet("/sendCodeServlet")
public class SendCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取邮箱
        String email = req.getParameter("email");
        //生成验证码
        String code = CaptchaUtil.generate();
        //发送邮件
        EmailUtil.send(email,code);
        //将验证码保存
        HttpSession session = req.getSession();
        session.setAttribute("code",code);
        //返回成功信息
        resp.getWriter().write("验证码已发送");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
