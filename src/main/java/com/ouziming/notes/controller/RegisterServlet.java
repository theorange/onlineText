package com.ouziming.notes.controller;

import com.ouziming.notes.service.UserService;
import com.ouziming.notes.util.MD5Until;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author ou`zi`ming
 * @date 2023年04月17日13时
 * discription 注册请求
 */
@MultipartConfig
@WebServlet("/registerservlet")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取表单数据
        String nickname = req.getParameter("username");
        String account = req.getParameter("account");
        String password = req.getParameter("password");
        Part avatarPart = req.getPart("avatar");
        String email = req.getParameter("email");
        // 处理上传文件
        InputStream avatarData = avatarPart.getInputStream();
        ByteArrayOutputStream avatarOutput = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = avatarData.read(buffer)) != -1) {
            avatarOutput.write(buffer, 0, len);
        }
        byte[] avatarBytes = avatarOutput.toByteArray();
        //创建用户
        if(UserService.addUser(nickname,account, MD5Until.md5(password),System.currentTimeMillis(),avatarBytes,email)==1){
            //注册成功
            resp.sendRedirect("login.html");
            System.out.println("注册成功");
        }else {
            //注册失败
            System.out.println("注册失败");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
