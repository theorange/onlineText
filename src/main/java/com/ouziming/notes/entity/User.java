package com.ouziming.notes.entity;

/**
 * @author ou`zi`ming
 * @date 2023年04月19日08时
 * discription
 */
public class User {
    private int id;
    private String userName;
    private String account;
    private String password;
    private Double money;
    private Long enrollTime;
    private byte[] headShot;
    private String email;

    public User() {
    }
    public User(int id, String userName, String account, String password, Double money, Long enrollTime, byte[] headShot, String email) {
        this.id = id;
        this.userName = userName;
        this.account = account;
        this.password = password;
        this.money = money;
        this.enrollTime = enrollTime;
        this.headShot = headShot;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Long getEnrollTime() {
        return enrollTime;
    }

    public void setEnrollTime(Long enrollTime) {
        this.enrollTime = enrollTime;
    }

    public byte[] getHeadShot() {
        return headShot;
    }

    public void setHeadShot(byte[] headShot) {
        this.headShot = headShot;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
