package com.ouziming.notes.entity;

/**
 * @author ou`zi`ming
 * @date 2023年04月18日22时
 * discription
 */
public class Goods {
    private int id;
    private double price;
    private String name;
    private String descrip;
    private int number;
    private int mfrs;
    private Double score;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getMfrs() {
        return mfrs;
    }

    public void setMfrs(int mfrs) {
        this.mfrs = mfrs;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
