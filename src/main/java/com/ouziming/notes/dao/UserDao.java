package com.ouziming.notes.dao;

import com.ouziming.notes.util.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author ou`zi`ming
 * @date 2023年04月16日18时
 * discription 数据库用户数据的操作
 */
public class UserDao {
    /**
     * 根据账号密码查询用户编号
     * @param account 账号
     * @param password 密码
     * @return 用户编号
     */
    public static int selectIdFromAccountAndPassword(String account,String password){
        String sql = "SELECT user.userId FROM user WHERE user.account = ? AND user.ppassword = ?";
        Connection conn = JdbcUtil.getConnection();
        PreparedStatement ps;
        ResultSet rs;
        int id = 0;
        try {
            if (conn == null){
                return 0;
            }else {
                ps = conn.prepareStatement(sql);
                //设置参数
                ps.setString(1,account);
                ps.setString(2,password);
                //获取结果
                rs = ps.executeQuery();
                if (rs.next()){
                    id = rs.getInt(1);
                }
                //关闭资源
                conn.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

    /**
     * 根据id查询用户
     * @param
     * @return 用户信息封装类
     */
//    public static DtoUser searchOnId(int id){
//
//    }
      public static int addUser(String userName,String accoount,String password,Long enrollTime,byte[] headShot,String email){
          String sql = "INSERT INTO `user`(username,account,ppassword,enrolltime,headshot,email)VALUES(?,?,?,?,?,?)";
          Connection conn = JdbcUtil.getConnection();
          PreparedStatement ps;
          //返回值受影响的行数
          int row = 0;
          try {
              if (conn == null){
                  return 0;
              }
              ps = conn.prepareStatement(sql);
              //设置参数
              ps.setString(1,userName);
              ps.setString(2,accoount);
              ps.setString(3,password);
              ps.setLong(4,enrollTime);
              ps.setBytes(5,headShot);
              ps.setString(6,email);
              //返回结果
              row = ps.executeUpdate();
              //关闭资源
              conn.close();
          }catch (Exception e){
              e.printStackTrace();
          }
          return row;
      }
}
